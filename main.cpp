#include "benchmark/benchmark.h"
#include "functions.cpp"

static void BM_vector_fixed(benchmark::State &state)
{
    for (auto _ : state)
        auto val = sum_of_vector_squared_fixed(state.range(0));
}

static void BM_vector(benchmark::State &state)
{
    for (auto _ : state)
        auto val = sum_of_vector_squared(state.range(0));
}

static void BM_list(benchmark::State &state)
{
    for (auto _ : state)
        auto val = sum_of_list_squared(state.range(0));
}

BENCHMARK(BM_vector_fixed)->Range(1, 1000000);
BENCHMARK(BM_vector)->Range(1, 1000000);
BENCHMARK(BM_list)->Range(1, 1000000);

BENCHMARK_MAIN();