#include <vector>
#include <list>
#include <array>
#include <iterator>
// #include <cstdint>
// #include <random>

// uint64_t sum_of_array_squared(int num_entries)
// {
//     std::array<uint64_t> simple_numbers(num_entries);
//     std::array<uint64_t> square_numbers(num_entries);
//     std::uint64_t sum = 0;
//     for (int iter = 0; iter != num_entries; ++iter)
//     {
//         simple_numbers[iter] = rand();
//         square_numbers[iter] = simple_numbers[iter] ^ 2;
//         sum += square_numbers[iter];
//     }
//     return sum;
// }

uint64_t sum_of_vector_squared(int num_entries)
{
    std::vector<uint64_t> simple_numbers;
    std::vector<uint64_t> square_numbers;
    std::uint64_t sum = 0;
    for (int iter = 0; iter != num_entries; ++iter)
        simple_numbers.push_back(rand());
    for (int iter = 0; iter != num_entries; ++iter)
        square_numbers.push_back(simple_numbers[iter] ^ 2);
    for (int iter = 0; iter != num_entries; ++iter)
        sum += square_numbers[iter];
    return sum;
}
uint64_t sum_of_vector_squared_fixed(int num_entries)
{
    std::vector<uint64_t> simple_numbers(num_entries);
    std::vector<uint64_t> square_numbers(num_entries);
    std::uint64_t sum = 0;
    for (int iter = 0; iter != num_entries; ++iter)
        simple_numbers[iter] = rand();
    for (int iter = 0; iter != num_entries; ++iter)
        square_numbers[iter] = simple_numbers[iter] ^ 2;
    for (int iter = 0; iter != num_entries; ++iter)
        sum += square_numbers[iter];
    return sum;
}
uint64_t sum_of_list_squared(int num_entries)
{
    std::list<uint64_t> simple_numbers;
    uint64_t sum = 0;
    std::list<uint64_t>::iterator list_iter;
    std::list<uint64_t> square_numbers;
    for (int iter = 0; iter != num_entries; ++iter)
        simple_numbers.push_back(rand());
    list_iter = simple_numbers.begin();
    for (int iter = 0; iter != num_entries; ++iter)
    {
        square_numbers.push_back(*list_iter ^ 2);
        ++list_iter;
    }
    list_iter = simple_numbers.begin();
    for (int iter = 0; iter != num_entries; ++iter)
    {
        sum += *list_iter;
        ++list_iter;
    }
    return sum;
}
